class Progress(val name: String, val unit: String) {
    private var start: Long = 0
    private var pos: Long = 0
    private var size: Long = 0

    @Synchronized
    fun reset(size: Long) {
        this.start = System.currentTimeMillis()
        this.pos = 0
        this.size = size
    }

    @Synchronized
    fun add(delta: Long, shadow: Boolean) {
        if (shadow) {
            start = System.currentTimeMillis()
        }
        pos += delta
    }

    @Synchronized
    fun progress() = Stat(start, pos, size)

    data class Stat(
        val start: Long,
        val pos: Long,
        val size: Long
    )
}