package streaming

import java.security.MessageDigest

class Hashing : StreamSubscriber {
    private val digest = MessageDigest.getInstance("SHA-256")

    override fun onData(arr: ByteArray, off: Int, len: Int) {
        digest.update(arr, off, len)
    }

    fun result(): ByteArray = digest.digest()
}