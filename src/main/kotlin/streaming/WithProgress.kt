package streaming

import Progress

class WithProgress(private val progress: Progress) : StreamSubscriber {
    override fun onData(arr: ByteArray, off: Int, len: Int) {
        progress.add(len.toLong(), false)
    }
}