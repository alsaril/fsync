package streaming

import java.io.EOFException
import java.io.InputStream

fun InputStream.observe(limit: Long? = null) = StreamingBuilder(this, limit)

class StreamingBuilder(
    private val inputStream: InputStream,
    private val limit: Long?
) {
    private val buffer = ByteArray(50 * 1024 * 1024)
    private val subscribers = mutableListOf<StreamSubscriber>()

    fun subscribe(subscriber: StreamSubscriber): StreamingBuilder {
        subscribers.add(subscriber)
        return this
    }

    fun stream() {
        var count = 0L

        while (limit == null || count < limit) {
            val read = inputStream.read(buffer, 0, remaining(count))
            if (read == -1) {
                if (limit == null) break
                throw EOFException()
            }

            subscribers.forEach { it.onData(buffer, 0, read) }
            count += read
        }
    }

    private fun remaining(read: Long): Int {
        if (limit == null) return buffer.size
        return (limit - read).coerceAtMost(buffer.size.toLong()).toInt()
    }
}

interface StreamSubscriber {
    fun onData(arr: ByteArray, off: Int, len: Int)
}