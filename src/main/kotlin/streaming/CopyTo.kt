package streaming

import java.io.OutputStream

class CopyTo(
    private val outputStream: OutputStream
) : StreamSubscriber {
    override fun onData(arr: ByteArray, off: Int, len: Int) {
        outputStream.write(arr, off, len)
    }
}