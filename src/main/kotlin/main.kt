import client.Client
import org.apache.commons.cli.CommandLine
import org.apache.commons.cli.DefaultParser
import org.apache.commons.cli.Options
import server.LocalServer
import server.RemoteServer
import kotlin.io.path.Path


fun main(args: Array<String>) {
    val options = Options().apply {
        addRequiredOption("m", "mode", true, "mode")
        addRequiredOption("r", "root", true, "root")
        addOption("h", "host", true, "remote host")
        addRequiredOption("p", "port", true, "local/remote port")
    }
    val parser = DefaultParser()
    val line = parser.parse(options, args)
    when (line.getOptionValue("mode")) {
        "client" -> startClient(line)
        "server" -> startServer(line)
    }
}

private fun startClient(line: CommandLine) {
    val root = line.getOptionValue("root")
    val host: String? = line.getOptionValue("host")
    val port = line.getOptionValue("port").toInt()

    RemoteServer(host, port).use {
        Client(Path(root), it).start()
    }
}

private fun startServer(line: CommandLine) {
    val root = line.getOptionValue("root")
    val host: String? = line.getOptionValue("host")
    val port = line.getOptionValue("port").toInt()

    LocalServer(host, port, Path(root)).use(LocalServer::start)
}