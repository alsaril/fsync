package index

import java.io.BufferedInputStream
import java.nio.file.Path
import java.util.zip.InflaterInputStream
import kotlin.io.path.inputStream

class FileIndex(private val path: Path, private val compressed: Boolean = false): Index {
    override fun <T, C : DirectoryCollector<T>> visit(visitor: IndexVisitor<T, C>): T {
        return fileInputStream(path, compressed).use { `is` ->
            val deserializer = ByteTagDeserializer(`is`)
            val header = deserializer.read()
            require(header is BeginTag)
            visitor.begin(header.fileCount, header.fullSize)
            val result = readTag(deserializer, visitor) ?: throw IllegalStateException("Unexpected end dir tag as root")
            require(deserializer.read() == EndTag)
            visitor.end()
            result
        }
    }

    private fun fileInputStream(path: Path, compressed: Boolean) =
        path.inputStream()
            .let(::BufferedInputStream)
            .let { if (compressed) InflaterInputStream(it) else it }

    private fun <T, C : DirectoryCollector<T>> readTag(deserializer: TagDeserializer, visitor: IndexVisitor<T, C>): T? {
        return when (val tag = deserializer.read()) {
            is FileTag -> visitor.file(tag.name, tag.size)
            is StartDirectoryTag -> readDirectory(tag, deserializer, visitor)
            EndDirectoryTag -> null
            else -> throw IllegalStateException("Unexpected tag: $tag")
        }
    }

    private fun<T, C : DirectoryCollector<T>> readDirectory(tag: StartDirectoryTag, deserializer: TagDeserializer, visitor: IndexVisitor<T, C>): T {
        val collector = visitor.directoryBegin(tag.name)
        while (true) {
            collector.add(readTag(deserializer, visitor) ?: break)
        }
        return visitor.directoryEnd(collector)
    }
}
