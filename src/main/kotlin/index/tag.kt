package index

sealed interface Tag

data class BeginTag(
    val fileCount: Long,
    val fullSize: Long
) : Tag

data object EndTag: Tag

data class FileTag(
    val name: String,
    val size: Long,
) : Tag

data class StartDirectoryTag(
    val name: String,
) : Tag

data object EndDirectoryTag : Tag