package index

interface IndexVisitor<T, C : DirectoryCollector<T>> {
    fun begin(fileCount: Long, fullSize: Long)
    fun end()
    fun directoryBegin(name: String): C
    fun directoryEnd(collector: C): T
    fun file(name: String, size: Long): T
}

interface DirectoryCollector<T> {
    fun add(entry: T)
}

object NoOpCollector : DirectoryCollector<Unit> {
    override fun add(entry: Unit) = Unit
}