package index

import index.TagType.*
import index.TagType.Companion.asType
import java.io.DataInputStream
import java.io.DataOutputStream
import java.io.InputStream
import java.io.OutputStream

interface TagSerializer {
    fun write(tag: Tag)
}

interface TagDeserializer {
    fun read(): Tag?
}

enum class TagType(val code: Int) {
    BEGIN(0x00),
    END(0x01),
    FILE(0x02),
    START_DIRECTORY(0x03),
    END_DIRECTORY(0x04);

    companion object {
        private val lookup = entries.associateBy { it.code }

        fun Int.asType() = lookup[this]
    }
}

class ByteTagSerializer(outputStream: OutputStream) : TagSerializer {
    private val dos = DataOutputStream(outputStream)

    override fun write(tag: Tag) {
        when (tag) {
            is BeginTag -> writeBegin(tag)
            is EndTag -> dos.write(END.code)
            is FileTag -> writeFile(tag)
            is StartDirectoryTag -> writeDirectory(tag)
            EndDirectoryTag -> dos.write(END_DIRECTORY.code)
        }
    }

    private fun writeBegin(tag: BeginTag) = with(tag) {
        dos.write(BEGIN.code)
        dos.writeLong(fileCount)
        dos.writeLong(fullSize)
    }

    private fun writeFile(tag: FileTag) = with(tag) {
        dos.write(FILE.code)
        dos.writeUTF(name)
        dos.writeLong(size)
    }

    private fun writeDirectory(tag: StartDirectoryTag) = with(tag) {
        dos.write(START_DIRECTORY.code)
        dos.writeUTF(name)
    }
}

class ByteTagDeserializer(inputStream: InputStream) : TagDeserializer {
    private val dis = DataInputStream(inputStream)

    override fun read(): Tag? {
        val code = dis.read()
        if (code == -1) return null

        val type = code.asType() ?: throw IllegalArgumentException("No tag with code $code is defined")
        return when (type) {
            BEGIN -> readBegin()
            END -> EndTag
            FILE -> readFile()
            START_DIRECTORY -> readDirectory()
            END_DIRECTORY -> EndDirectoryTag
        }
    }

    private fun readBegin() = BeginTag(
        fileCount = dis.readLong(),
        fullSize = dis.readLong(),
    )

    private fun readFile() = FileTag(
        name = dis.readUTF(),
        size = dis.readLong(),
    )

    private fun readDirectory() = StartDirectoryTag(
        name = dis.readUTF(),
    )
}