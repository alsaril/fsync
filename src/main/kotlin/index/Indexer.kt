package index

import java.io.BufferedOutputStream
import java.io.RandomAccessFile
import java.nio.file.Files.walkFileTree
import java.nio.file.Path
import kotlin.io.path.outputStream

class Indexer(private val root: Path) {
    fun index(output: Path) {
        val stats = BufferedOutputStream(output.outputStream()).use { os ->
            val serializer = ByteTagSerializer(os)
            BuildIndexVisitor(serializer::write).use {
                walkFileTree(root, it)
                it.stats()
            }
        }

        // patch stats
        RandomAccessFile(output.toFile(), "rw").use {
            it.seek(1) // begin marker
            it.writeLong(stats.first)
            it.writeLong(stats.second)
        }
    }
}