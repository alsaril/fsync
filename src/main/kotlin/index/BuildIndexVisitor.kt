package index

import java.io.IOException
import java.nio.file.FileVisitResult
import java.nio.file.FileVisitResult.CONTINUE
import java.nio.file.FileVisitor
import java.nio.file.Path
import java.nio.file.attribute.BasicFileAttributes
import kotlin.io.path.fileSize
import kotlin.io.path.name

class BuildIndexVisitor(
    private val tagsConsumer: (Tag) -> Unit,
) : FileVisitor<Path>, AutoCloseable {

    private var count = 0L
    private var size = 0L

    init {
        tagsConsumer(BeginTag(0, 0)) // dummy header
    }

    fun stats() = count to size // real header

    override fun close() {
        tagsConsumer(EndTag)
    }

    override fun preVisitDirectory(dir: Path, attrs: BasicFileAttributes): FileVisitResult {
        tagsConsumer(StartDirectoryTag(name = dir.name))

        return CONTINUE
    }

    override fun visitFile(file: Path, attrs: BasicFileAttributes): FileVisitResult {
        val fileSize = file.fileSize()

        tagsConsumer(FileTag(file.name, fileSize))

        count++
        size += fileSize

        return CONTINUE
    }

    override fun visitFileFailed(file: Path, exc: IOException): FileVisitResult {
        println("Exception in visit file")
        exc.printStackTrace(System.out)

        return CONTINUE
    }

    override fun postVisitDirectory(dir: Path, exc: IOException?): FileVisitResult {
        exc?.let { throw it }

        tagsConsumer(EndDirectoryTag)

        return CONTINUE
    }
}