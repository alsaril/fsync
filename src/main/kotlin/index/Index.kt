package index

interface Index {
    fun <T, C : DirectoryCollector<T>> visit(visitor: IndexVisitor<T, C>): T
}