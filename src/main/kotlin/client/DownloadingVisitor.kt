package client

import Progress
import combine
import index.IndexVisitor
import index.NoOpCollector
import server.api.Server
import java.io.BufferedOutputStream
import java.nio.file.Files.createDirectories
import java.nio.file.Path
import kotlin.io.path.Path
import kotlin.io.path.outputStream

class DownloadingVisitor(
    private val root: Path,
    private val server: Server,
    private val stateHandler: StateHandler,
) : IndexVisitor<Unit, NoOpCollector> {

    private val countTracker = Progress("count", "")
    private val sizeTracker = Progress("size", "B")
    private val fileTracker = Progress("file", "B")

    private val nameStack = mutableListOf<Path>()

    private var fileCount = 0L
    private var fullSize = 0L

    private val startCounter = stateHandler.loadState() ?: 0
    private var counter = 0L
    private var transferred = 0L

    private fun remotePath() = nameStack.combine()
    private fun localPath() = root.resolve(remotePath())

    val trackers = listOf(countTracker, sizeTracker, fileTracker)

    override fun begin(fileCount: Long, fullSize: Long) {
        createDirectories(root)

        this.fileCount = fileCount
        this.fullSize = fullSize

        countTracker.reset(fileCount)
        sizeTracker.reset(fullSize)
    }

    override fun end() = Unit

    override fun directoryBegin(name: String): NoOpCollector {
        checkAmbiguity(name)
        nameStack.add(Path(name))
        createDirectories(localPath())
        return NoOpCollector
    }

    override fun file(name: String, size: Long) {
        checkAmbiguity(name)
        if (counter < startCounter) {
            updateCounters(size, shadow = true)
            return
        }
        val image = server.getFile(remotePath().resolve(name), fileTracker)
        require(image.size == size)
        BufferedOutputStream(localPath().resolve(name).outputStream()).use(image::writeTo)
        updateCounters(size)
        stateHandler.saveState(counter)
    }

    private fun updateCounters(size: Long, shadow: Boolean = false) {
        counter++
        transferred += size

        countTracker.add(1, shadow)
        sizeTracker.add(size, shadow)
    }

    override fun directoryEnd(collector: NoOpCollector) {
        nameStack.removeLast()
    }

    private fun checkAmbiguity(name: String) {
        require(name != "." && name != "..")
    }
}