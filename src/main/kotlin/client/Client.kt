package client

import index.FileIndex
import index.Index
import org.joda.time.Duration.standardDays
import org.joda.time.Duration.standardSeconds
import org.joda.time.Period
import org.joda.time.format.PeriodFormatterBuilder
import server.api.Server
import java.io.BufferedOutputStream
import java.io.File
import java.io.FileOutputStream
import java.lang.System.currentTimeMillis
import java.lang.Thread.interrupted
import java.lang.Thread.sleep
import java.nio.file.Path
import kotlin.concurrent.thread
import kotlin.io.path.Path


class Client(
    private val root: Path,
    private val server: Server,
) {
    private val indexPath = "index"
    private val counterPath = "state"

    fun start() {
        val index = openIndex()
        val stateHandler = StateHandler(counterPath)
        val visitor = DownloadingVisitor(root, server, stateHandler)
        listenForProgress(visitor)
        index.visit(visitor)
    }

    private fun openIndex(): Index {
        val file = File(indexPath)
        if (!file.exists()) {
            downloadIndex()
        }

        return FileIndex(Path(indexPath), false)
    }

    private fun downloadIndex() {
        val image = server.index()
        BufferedOutputStream(FileOutputStream(indexPath)).use(image::writeTo)
    }

    private fun listenForProgress(visitor: DownloadingVisitor) = thread(isDaemon = true) {
        while (!interrupted()) {
            sleep(1000)
            val stats = visitor.trackers.map { it to it.progress() }
            val time = currentTimeMillis()
            println("==================")
            stats.forEach { (tracker, stat) ->
                val percent = (100.0 * stat.pos / stat.size).toInt()
                val speed = (1000.0 * stat.pos / (time - stat.start)).toLong()
                val left = ((stat.size - stat.pos).toFloat() * (time - stat.start) / stat.pos).toLong()
                println("${tracker.name}: ${shortNum(stat.pos)}${tracker.unit}/${shortNum(stat.size)}${tracker.unit}, $percent%, ${shortNum(speed)}${tracker.unit}/s, ${formatDuration(left)}")
            }
            println("==================\n")
        }
    }

    private val suffixes = listOf("", "k", "M", "G", "T", "E")

    private fun shortNum(value: Long): String {
        require(value >= 0)
        var remainder = value
        val suffixIterator = suffixes.iterator()
        while (remainder >= 1000) {
            remainder /= 1000
            suffixIterator.next()
        }
        return "$remainder${suffixIterator.next()}"
    }

    private val durationFormatter = PeriodFormatterBuilder()
        .appendDays()
        .appendSuffix("d ")
        .appendHours()
        .appendSuffix("h ")
        .appendMinutes()
        .appendSuffix("m ")
        .appendSeconds()
        .appendSuffix("s ")
        .toFormatter()

    private fun formatDuration(millis: Long): String {
        if (millis > standardDays(365).millis) return "inf"
        if (millis < standardSeconds(1).millis) return "<1s"
        val period = Period(millis).normalizedStandard()
        return durationFormatter.print(period)
    }
}