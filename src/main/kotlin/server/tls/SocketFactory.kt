package server.tls

import java.net.ServerSocket
import java.net.Socket
import javax.net.ssl.SSLServerSocket
import javax.net.ssl.SSLServerSocketFactory
import javax.net.ssl.SSLSocket
import javax.net.ssl.SSLSocketFactory

object SocketFactory {
    fun tlsSocket(host: String?, port: Int): Socket {
        return host?.let { tlsClientSocket(it, port) } ?: tlsServerSocket(port).use { it.accept() }
    }

    fun tlsClientSocket(host: String, port: Int): Socket {
        val socket = SSLSocketFactory.getDefault().createSocket(host, port) as SSLSocket
        socket.enabledCipherSuites = arrayOf("TLS_AES_128_GCM_SHA256")
        socket.enabledProtocols = arrayOf("TLSv1.3")
        return socket
    }

    fun tlsServerSocket(port: Int): ServerSocket {
        val socket = SSLServerSocketFactory.getDefault().createServerSocket(port) as SSLServerSocket
        socket.needClientAuth = true
        socket.enabledCipherSuites = arrayOf("TLS_AES_128_GCM_SHA256")
        socket.enabledProtocols = arrayOf("TLSv1.3")
        return socket
    }
}