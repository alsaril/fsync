package server.api

import Progress
import java.io.OutputStream
import java.nio.file.Path

interface Server : AutoCloseable {
    fun index(): FileImage
    fun getFile(path: Path, progress: Progress): FileImage

    interface FileImage {
        val size: Long
        fun writeTo(outputStream: OutputStream)
    }
}