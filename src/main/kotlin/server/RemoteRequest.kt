package server

enum class RemoteRequest(val code: Int) {
    CLOSE(0x00),
    INDEX(0x01),
    FILE(0x02);

    companion object {
        private val lookup = RemoteRequest.entries.associateBy { it.code }

        fun Int.asRequest() = lookup[this]
    }
}