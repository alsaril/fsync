package server

import Progress
import server.RemoteRequest.*
import server.api.Server
import server.api.Server.FileImage
import server.tls.SocketFactory.tlsSocket
import streaming.CopyTo
import streaming.Hashing
import streaming.WithProgress
import streaming.observe
import java.io.DataInputStream
import java.io.DataOutputStream
import java.io.OutputStream
import java.nio.file.Path
import kotlin.io.path.pathString


class RemoteServer(host: String?, port: Int) : Server {
    private val socket = tlsSocket(host, port)
    private val dis = DataInputStream(socket.getInputStream())
    private val dos = DataOutputStream(socket.getOutputStream())

    private var contentExpected = false

    override fun close() {
        dos.write(CLOSE.code)
        dos.flush()
        socket.close()
    }

    override fun index(): FileImage {
        require(!contentExpected)
        dos.write(INDEX.code)
        dos.flush()
        return readImage(Progress("index", "B"))
    }

    override fun getFile(path: Path, progress: Progress): FileImage {
        require(!contentExpected)
        dos.write(FILE.code)
        dos.writeUTF(path.pathString)
        dos.flush()
        return readImage(progress)
    }

    private fun readImage(progress: Progress): FileImage {
        val size = dis.readLong()
        contentExpected = true
        return RemoteCheckedFileImage(size, progress)
    }

    private inner class RemoteCheckedFileImage(
        override val size: Long,
        private val progress: Progress
    ) : FileImage {
        private var finished = false

        override fun writeTo(outputStream: OutputStream) {
            require(contentExpected && !finished)
            progress.reset(size)
            val hashing = Hashing()
            dis.observe(size)
                .subscribe(CopyTo(outputStream))
                .subscribe(hashing)
                .subscribe(WithProgress(progress))
                .stream()
            val localHash = hashing.result()
            val remoteHash = ByteArray(localHash.size).also(dis::readFully)
            require(localHash.contentEquals(remoteHash))
            finished = true
            contentExpected = false
        }
    }
}