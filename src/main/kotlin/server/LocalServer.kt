package server

import checkPath
import index.Indexer
import server.RemoteRequest.*
import server.RemoteRequest.Companion.asRequest
import server.tls.SocketFactory.tlsSocket
import streaming.CopyTo
import streaming.Hashing
import streaming.observe
import java.io.BufferedInputStream
import java.io.DataInputStream
import java.io.DataOutputStream
import java.nio.file.Path
import kotlin.io.path.Path
import kotlin.io.path.deleteExisting
import kotlin.io.path.fileSize
import kotlin.io.path.inputStream


class LocalServer(host: String?, port: Int, private val root: Path) : AutoCloseable {
    private val socket = tlsSocket(host, port)
    private val dis = DataInputStream(socket.getInputStream())
    private val dos = DataOutputStream(socket.getOutputStream())

    fun start() {
        while (true) {
            val code = dis.read()
            if (code == -1) return
            val request = code.asRequest() ?: throw IllegalArgumentException("No request with code $code is defined")
            when (request) {
                CLOSE -> return
                INDEX -> index()
                FILE -> sendFile(checkPath(root, root.parent.resolve(dis.readUTF())))
            }
        }
    }

    private fun index() {
        val index = Path(".", "idx_tmp_" + System.currentTimeMillis())
        Indexer(root).index(index)
        sendFile(index)
        index.deleteExisting()
    }

    private fun sendFile(path: Path) {
        val size = path.fileSize()
        dos.writeLong(size)
        val hash = BufferedInputStream(path.inputStream()).use {
            val hashing = Hashing()
            it.observe(size)
                .subscribe(CopyTo(dos))
                .subscribe(hashing)
                .stream()
            hashing.result()
        }
        dos.write(hash)
        dos.flush()
    }

    override fun close() {
        socket.close()
    }
}