import java.nio.file.Path
import kotlin.io.path.Path

fun checkPath(root: Path, path: Path): Path {
    require(path.normalize().startsWith(root.normalize()))
    return path
}

fun Iterable<Path>.combine() = reduceOrNull { left, right -> left.resolve(right) } ?: Path("")